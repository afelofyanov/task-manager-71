package ru.tsc.felofyanov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.tsc.felofyanov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/tasks")
public interface ITaskEndpoint {

    @WebMethod
    @GetMapping("/count")
    long count();

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "task")
            @RequestBody Task task
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @WebParam(name = "tasks")
            @RequestBody List<Task> tasks
    );

    @WebMethod
    @DeleteMapping("/clear")
    void clear();

    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/findAll")
    List<Task> findAll();

    @WebMethod
    @GetMapping("/findById/{id}")
    Task findById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/save")
    Task save(
            @WebParam(name = "task")
            @RequestBody Task task
    );

    @WebMethod
    @PostMapping("/saveAll")
    List<Task> saveAll(
            @WebParam(name = "tasks")
            @RequestBody List<Task> tasks
    );
}
