package ru.tsc.felofyanov.tm.exception;

public final class ModelEmptyException extends AbstractException {

    public ModelEmptyException() {
        super("Error! Model is empty...");
    }
}
