package ru.tsc.felofyanov.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class User extends AbstractModel {

    private static final long serialVersionUID = 1;

    @Nullable
    @Column(nullable = false, unique = true)
    private String login;

    @Nullable
    @JsonIgnore
    @Column(nullable = false, name = "password")
    private String passwordHash;

    @Nullable
    @Column(unique = true)
    private String email;

    @Nullable
    @Column(name = "fst_name")
    private String firstName;

    @Nullable
    @Column(name = "mid_name")
    private String middleName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @JsonIgnore
    @XmlTransient
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Role> roles = new ArrayList<>();

    @NotNull
    @Column(nullable = false)
    private Boolean locked = false;

    public User(@Nullable final String login, @Nullable final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }
}

