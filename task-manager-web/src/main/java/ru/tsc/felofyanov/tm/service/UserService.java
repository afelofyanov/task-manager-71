package ru.tsc.felofyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.felofyanov.tm.api.repository.IUserRepository;
import ru.tsc.felofyanov.tm.api.service.IUserService;
import ru.tsc.felofyanov.tm.enumerated.RoleType;
import ru.tsc.felofyanov.tm.exception.LoginEmptyException;
import ru.tsc.felofyanov.tm.exception.RoleEmptyException;
import ru.tsc.felofyanov.tm.model.Role;
import ru.tsc.felofyanov.tm.model.User;

import javax.annotation.PostConstruct;
import java.util.Collections;

@Service
public class UserService implements IUserService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private IUserRepository repository;

    @PostConstruct
    private void init() {
        initUser("admin", "admin", RoleType.ADMIN);
        initUser("test", "test", RoleType.USUAL);
    }

    private void initUser(@Nullable final String login, @Nullable final String password, @Nullable final RoleType role) {
        @Nullable final User user = findByLogin(login);
        if (user != null) return;
        createUser(login, password, role);
    }

    @Override
    @Transactional
    public void createUser(@Nullable final String login, @Nullable final String password, @Nullable final RoleType roleType) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new LoginEmptyException();
        if (roleType == null) throw new RoleEmptyException();

        @NotNull final String passwordHash = passwordEncoder.encode(password);
        @NotNull final User user = new User(login, passwordHash);

        @NotNull final Role role = new Role();
        role.setUser(user);
        role.setRoleType(roleType);
        user.setRoles(Collections.singletonList(role));
        repository.save(user);
    }

    @Override
    @Nullable
    @Transactional
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = repository.findFirstByLogin(login);
        return user;
    }
}
