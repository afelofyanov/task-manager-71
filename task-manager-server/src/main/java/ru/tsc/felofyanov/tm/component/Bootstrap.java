package ru.tsc.felofyanov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.api.repository.IDomainRepository;
import ru.tsc.felofyanov.tm.api.service.IAuthService;
import ru.tsc.felofyanov.tm.api.service.IDomainService;
import ru.tsc.felofyanov.tm.api.service.ILoggerService;
import ru.tsc.felofyanov.tm.api.service.IPropertyService;
import ru.tsc.felofyanov.tm.api.service.dto.IProjectDTOService;
import ru.tsc.felofyanov.tm.api.service.dto.IProjectTaskDTOService;
import ru.tsc.felofyanov.tm.api.service.dto.ITaskDTOService;
import ru.tsc.felofyanov.tm.api.service.dto.IUserDTOService;
import ru.tsc.felofyanov.tm.endpoint.AbstractEndpoint;
import ru.tsc.felofyanov.tm.listener.EntityListener;
import ru.tsc.felofyanov.tm.listener.OperationEventListener;
import ru.tsc.felofyanov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Getter
@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Autowired
    private IProjectTaskDTOService projectTaskService;

    @NotNull
    @Autowired
    private IUserDTOService userService;

    @NotNull
    @Autowired
    private IAuthService authService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private IDomainRepository domainRepository;

    @NotNull
    @Autowired
    private IDomainService domainService;

    public void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    public void registry(Object endpoint) {
        @NotNull final String host = "localhost";
        @NotNull final String port = "8080";
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void run() {
        initEndpoints();
        initPID();
        initJMS();
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER SERVER STOPPED **");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    @SneakyThrows
    public void initJMS() {
        BasicConfigurator.configure();
        EntityListener.setConsumer(new OperationEventListener());
    }
}
