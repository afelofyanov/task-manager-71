package ru.tsc.felofyanov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.model.AbstractWbs;
import ru.tsc.felofyanov.tm.model.User;

import java.util.List;

public interface IUserOwnerService<M extends AbstractWbs> extends IService<M> {

    @Nullable
    M create(@Nullable User user, @Nullable String name);

    @Nullable
    M create(@Nullable User user, @Nullable String name, @Nullable String description);

    @Nullable
    M createByUserId(@Nullable String userId, @Nullable String name);

    @Nullable
    M createByUserId(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull List<M> findAllByUserId(@Nullable String userId);

    void clearByUserId(@Nullable String userId);

    boolean existsByIdUserId(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIdUserId(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIndexByUserId(@Nullable String userId, @Nullable Integer index);

    M remove(@Nullable String userId, M model);

    @Nullable
    M removeByIdByUserId(@Nullable String userId, @Nullable String id);

    @Nullable
    M removeByIndexByUserId(@Nullable String userId, @Nullable Integer index);

    @NotNull
    M updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    M updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    M changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    M changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    long countByUserId(@Nullable String userId);
}
