package ru.tsc.felofyanov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataSaveYamlFasterXmlRequest extends AbstractUserRequest {

    public DataSaveYamlFasterXmlRequest(@Nullable String token) {
        super(token);
    }
}
