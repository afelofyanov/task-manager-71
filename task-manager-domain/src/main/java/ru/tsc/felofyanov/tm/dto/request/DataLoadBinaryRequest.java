package ru.tsc.felofyanov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataLoadBinaryRequest extends AbstractUserRequest {

    public DataLoadBinaryRequest(@Nullable String token) {
        super(token);
    }
}
