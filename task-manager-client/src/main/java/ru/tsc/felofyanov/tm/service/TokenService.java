package ru.tsc.felofyanov.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.felofyanov.tm.api.service.ITokenService;

@Getter
@Setter
@Service
public class TokenService implements ITokenService {

    @Nullable
    private String token;
}
