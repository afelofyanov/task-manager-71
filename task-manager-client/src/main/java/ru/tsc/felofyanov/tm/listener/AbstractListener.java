package ru.tsc.felofyanov.tm.listener;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.api.model.ICommand;
import ru.tsc.felofyanov.tm.api.service.ITokenService;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;

@Getter
@Setter
@Component
public abstract class AbstractListener implements ICommand {

    @NotNull
    @Autowired
    private ITokenService tokenService;

    @Nullable
    public abstract String getName();

    @Nullable
    public abstract String getArgument();

    @Nullable
    public abstract String getDescription();

    @Nullable
    public abstract Role[] getRoles();

    public abstract void handler(@NotNull final ConsoleEvent event);

    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    protected void setToken(@Nullable final String token) {
        tokenService.setToken(token);
    }

    @Override
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        @NotNull String result = "";
        if (name != null && !name.isEmpty())
            result += name;

        if (argument != null && !argument.isEmpty())
            result += ", " + argument;

        if (description != null && !description.isEmpty())
            result += " - " + description;
        return result;
    }
}
